#!/usr/bin/env python3
"""
# Created: Wed Jan 19 14:39:23 2022 (-0600)
# @author: jacobfaibussowitsch
"""
import sys

sys.path.append('/Users/jacobfaibussowitsch/NoSync/warp4py')

import os
import pathlib
import warp4py as warp

def main():
  warp.conf.set_library_executable(
    pathlib.Path(os.environ['WARP3D_HOME'])/'run_mac_os_x'/'warp3d_gfortran.omp'
  )

  model = warp.Model('example',interactive=True)

  interface_mat = warp.Material(
    model,'interface_exp','cohesive','exp1_intf',
    killable=True,
    delta_peak=0.000285,
    sig_peak=220,
    beta=1.0,
    compression_multiplier=15
  )
  model.view()
  return

if __name__ == '__main__':
  main()
