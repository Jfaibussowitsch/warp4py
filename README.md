# warp4py

WARP3D Script Generator from Python

## Installation

Install via [pip](https://pypi.org/project/warp4py/)

```sh
$ python3 -m pip install warp4py
```

Or clone the repository and install an editable copy from `setup.py`

```sh
$ git clone https://gitlab.com/Jfaibussowitsch/warp4py.git
$ cd warp4py
$ python3 -m pip install -e .
```

## Examples

See the top-level `examples/` directory for a comprehensive set of example sets

## Authors and acknowledgment

This project is supported by the [Center for Exascale-enabled Scramjet Design (CEESD)](https://ceesd.illinois.edu/) at the University of Illinois at Urbana-Champaign.

This material is based in part upon work supported by the Department of Energy, National Nuclear Security Administration, under Award Number DE-NA0003963.
