#!/usr/bin/env python3
"""
# Created: Sun Jan  9 14:15:18 2022 (-0500)
# @author: jacobfaibussowitsch
"""
import collections
from ._warpobject import WarpObjectBase
from ._util import to_arglist

class Material(WarpObjectBase):
  _typename = 'material'

  def __init__(self,model,name,*args,**kwargs):
    __doc__="""
    Define a material

    Parameters
    ----------
    model    : The model that owns this material
    name     : Name of the material, convertable to string
    *args    : The type of material, convertable to string e.g."'cohesive','exp1_intf'"
    **kwargs : The properties of the material (can be empty)
    """
    super().__init__(name)
    if not len(args):
      raise ValueError('Must define the type of material')
    self._model      = list(map(str,args))
    self._properties = collections.OrderedDict(**kwargs)
    model._add_object(self)
    return

  def _finalize(self):
    arg_list = to_arglist(self._properties)
    if arg_list:
      formatted_arg_list = [a+',' for a in arg_list[:-1]]+[arg_list[-1]]
    else:
      formatted_arg_list = []
    return [
      f'properties {" ".join(self._model)},',
      formatted_arg_list
    ]

  def set_properties(self,**kwargs):
    __doc__="""
    Set or update the properties for a material

    Parameters
    ----------
    **kwargs : The properties to update and their values. If the property already exists on the material, the new valye will override the existing value
    """
    self._properties.update(**kwargs)
    return
