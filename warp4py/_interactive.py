#!/usr/bin/env python3
"""
# Created: Wed Jan 19 14:45:17 2022 (-0600)
# @author: jacobfaibussowitsch
"""
import sys
import enum
import weakref
import asyncio
import multiprocessing as mp

from ._util import resolve_path

class QueueSignal(enum.Enum):
  EXIT_QUEUE     = enum.auto()
  PROCESS_OUTPUT = enum.auto()
  PROCESS_ERROR  = enum.auto()


async def process_main_(stdin_queue,stdout_queue,lock,executable):
  async def initialize_process(executable):
    return
  def lock_print(*args,**kwargs):
    if args:
      with lock:
        print(*args,**kwargs)
    return

  def push_item(kind,item):
    return stdout_queue.put((kind,item.decode() if isinstance(item,bytes) else item))

  def push_output(item):
    return push_item(QueueSignal.PROCESS_OUTPUT,item)

  def push_error(item):
    return push_item(QueueSignal.PROCESS_ERROR,item)

  async def process_data(stdin,stdout,data):
    data = str(data)
    if data and not data.isspace():
      if not data.endswith('\n'):
        data += '\n'
      stdin.write(data.encode())
      await stdin.drain()

      timeout    = 1
      got_return = False
      while True:
        try:
          line = await asyncio.wait_for(stdout.readline(),timeout)
        except asyncio.TimeoutError:
          if got_return:
            break
          # decrease timeout time by half, if we have timed out chances are
          # there is nothing to wait on
          timeout /= 2
        else:
          if line:
            push_output(line.decode())
            got_return = True
            timeout    = 1
          elif got_return:
            break
    return

  def flush_queue(queue):
    try:
      while True: # in case we had any work from the queue we need to release all of it
        queue.task_done()
    except ValueError:
      pass # task_done() called more times than get()
    return

  async def shutdown(process,exception=None):
    if exception is not None:
      try:
        # attempt to send the traceback back to parent
        push_error(str(exception))
      except:
        # if this fails then I guess we really are fucked
        push_error('UNKNOWN ERROR')
    try:
      stdout_data,stderr_data = await process.communicate(input='stop\n'.encode(),timeout=5)
    except:
      # whatever happens (be it a timeout or otherwise) we kill the process and finish the
      # cleanup
      process.kill()
      stdout_data,stderr_data = await process.communicate()
    if stderr_data:
      push_error(stderr_data)
    push_output(stdout_data)
    flush_queue(stdin_queue)
    return


  process = await asyncio.subprocess.create_subprocess_exec(
    executable,
    stdout=asyncio.subprocess.PIPE,stdin=asyncio.subprocess.PIPE,stderr=asyncio.subprocess.STDOUT
  )
  stdout = process.stdout
  stdin  = process.stdin

  try:
    while True:
      input_data = stdin_queue.get()
      if input_data == QueueSignal.EXIT_QUEUE:
        break

      await process_data(stdin,stdout,input_data)
      stdin_queue.task_done()

  except Exception as e:
    finalizer = shutdown(process,exception=e)
  else:
    finalizer = shutdown(process)
  await finalizer
  return


def process_main(*args):
  loop = asyncio.get_event_loop()
  ret  = loop.run_until_complete(process_main_(*args))
  loop.close()
  return

class WarpProcess(object):
  def __init__(self,executable):
    self._in_queue  = mp.JoinableQueue()
    self._out_queue = mp.Queue()
    self._lock      = mp.Lock()
    self._process   = mp.Process(
      target=process_main,args=(self._in_queue,self._out_queue,self._lock,executable),daemon=True
    )
    self._process.start()
    self._stdout    = []
    self._finalizer = weakref.finalize(self,self._finalize)
    return

  @property
  def finalized(self):
    return not self._finalizer.alive

  def finalize(self):
    return self._finalizer()

  def _finalize(self):
    self.send(QueueSignal.EXIT_QUEUE)
    self.synchronize()
    self._process.join()
    # if out:
    #   self._lock_print(''.join(out))
    return

  def __getattr__(self,attr):
    return getattr(self._process,attr)

  def _lock_print(self,*args,**kwargs):
    if args:
      with self._lock:
        print(*args,**kwargs)
    return

  def send(self,stdin_data):
    if not isinstance(stdin_data,QueueSignal):
      stdin_data = str(stdin_data)
    self._in_queue.put(stdin_data)
    return

  def synchronize(self):
    self._in_queue.join()

    stop_multiproc = False
    stdout_queue   = self._out_queue
    new_stdout     = []

    while not stdout_queue.empty():
      code,ret = stdout_queue.get()
      if code == QueueSignal.PROCESS_OUTPUT:
        new_stdout.append(ret)
      elif code == QueueSignal.PROCESS_ERROR:
        # while this does get recreated for every error, we do not want to needlessly
        # reinitialize it when no errors exist. If we get to this point however we no longer
        # care about performance as we are about to crash everything.
        error_bars = ''.join(('[ERROR]',85*'-','[ERROR]\n'))
        try:
          error_message = ret.join((error_bars,error_bars))
        except:
          error_message = ret
        print(error_message)
        stop_multiproc = True
    if stop_multiproc:
      self._process.kill()
      self._finalizer.detach()
      raise RuntimeError('Error in child process detected')

    if len(new_stdout):
      self._stdout.extend(new_stdout)
    return new_stdout

class LibConfig(object):
  executable = None

  @classmethod
  def set_library_executable(cls,path):
    __doc__="""
    Set the exact location of the WARP3D executable

    Parameters
    ----------
    path : path to an executable instance of the WARP3D interpreter
    """
    if cls.executable is not None:
      raise Exception(
        "executable file must be set before before using any direct functionalities in warp4py"
      )
    cls.executable = resolve_path(path,strict=True)
    return

  def _initialize_process(self):
    if self.executable is None:
      raise RuntimeError(
        'executable file must be set before before using any direct functionalities in warp4py'
      )

    return WarpProcess(self.executable)

conf = LibConfig()
